const { factorial, divCheck } = require('../src/util.js');

const { expect, assert } = require('chai');


describe('test_fun_factorials', function() {
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	})

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1);
	})

	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0);
		expect(product).to.equal(1);
	})

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4);
		assert.equal(product, 24);
	})

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10);
		expect(product).to.equal(3628800);
	})
});

describe('test_divisibility_by_5_or_7', function() {

	it('test_100_is_divisible_by_5', () => {
		const divisible = divCheck(100);
		expect(divisible).to.equal(true);
	})

	it('test_49_is_divisible_by_7', () => {
		const divisible = divCheck(100);
		expect(divisible).to.equal(true);
	})

	it('test_30_is_divisible_by_5', () => {
		const divisible = divCheck(30);
		assert.equal(divisible, true)
	})

	it('test_56_is_divisible_by_7', () => {
		const divisible = divCheck(56);
		assert.equal(divisible, true)
	})


})