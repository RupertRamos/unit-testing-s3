const names = {
	"Brandon": {
		"name" : "Brandon Boyd",
		"age" : 35
	}, 
	"Steve" : {
		"name" : "Steve Tyler",
		"age" : 56
	}
}


function factorial(n){
	if(typeof n !== 'number') return undefined; //remove this first
	if(n<0) return undefined; //remove this first
	if(n===0) return 1;
	if(n===1) return 1;
	return n * factorial(n-1);
}

function div_check(n) {
	if(n % 5 == 0) {
		return true
	} else if (n % 7 == 0) {
		return true
	} else {
		return false
	}
};

module.exports = {
	factorial: factorial,
	divCheck: div_check,
	names: names
}
